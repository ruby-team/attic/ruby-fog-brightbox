require 'rake/testtask'
Rake::TestTask.new(:minitest) do |test|
  test.test_files = FileList["spec/**/*_spec.rb"]
  test.verbose = false
  test.warning = false
  test.libs << 'spec'
end


task :default => [:minitest]
